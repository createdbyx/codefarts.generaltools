﻿Copyright (c) 2012 Codefarts 
contact@codefarts.com
http://www.codefarts.com/
All rights reserved.

General Tools is a collection of general purpose tools for unity that assists you in performing various tasks.

Documentation is available in the documentation folder "Documentation\GeneralToolsDocumentation.zip". 